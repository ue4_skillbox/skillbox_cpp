#include "Vector.h"
#include <iostream>
#include <string>

Vector::Vector()
{}

Vector::Vector(double x, double y, double z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

std::string Vector::ToString()
{
	return std::string("x: " + std::to_string(x) + ", y: " + std::to_string(y) + ", z : " + std::to_string(z));
}

double Vector::GetModule()
{
	return std::sqrt(x*x + y*y + z*z);
}

