#include <iostream>

class Vector
{
public:
	Vector();
	Vector(double x, double y, double z);
	std::string  ToString();
	double GetModule();
private:
	double x;
	double y;
	double z;
};