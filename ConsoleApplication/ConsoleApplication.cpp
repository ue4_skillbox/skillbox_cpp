﻿#include <iostream>

using namespace std;

class Animal 
{
public:
    virtual void Voice()
    {
        cout << "Sounds: Animal\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Sounds: Dog\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Sounds: Cat\n";
    }
};

class Rabbit : public Animal
{
public:
    void Voice() override
    {
        cout << "Sounds: Rabbit\n";
    }
};

enum AnimalType 
{
    dog,
    cat,
    rabbit,
    none
};

Animal* CreateAnimal()
{
    AnimalType type = static_cast<AnimalType>(rand() % none);
    switch (type)
    {
        case dog:
            return new Dog();
        case cat:
            return new Cat();
        case rabbit:
            return new Rabbit();
    }
}

int main()
{
    const int animalNumber = 10;
    Animal* animals[animalNumber];

    for (size_t i = 0; i < animalNumber; i++)
    {
        animals[i] = CreateAnimal();
    }

    for (size_t i = 0; i < animalNumber; i++)
    {
        animals[i]->Voice();
    }

    return 0;
}